﻿using System.Collections.Generic;

namespace AbbyyTestTask.Core.Builders
{
    public interface ICanonicalEquationBuilder
    {
        CanonicalEquation BuildCanonicalEquation(ICollection<EquationToken> tokens);
    }
}
