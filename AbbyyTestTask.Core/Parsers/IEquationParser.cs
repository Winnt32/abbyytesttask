﻿using System.Collections.Generic;

namespace AbbyyTestTask.Core.Parsers
{
    public interface IEquationParser
    {
        ICollection<EquationToken> Parse(string s);
    }
}
