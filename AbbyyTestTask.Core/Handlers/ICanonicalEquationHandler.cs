﻿namespace AbbyyTestTask.Core.Handlers
{
    public interface ICanonicalEquationHandler
    {
        CanonicalEquation Processing(string s);
    }
}