﻿using System;
using AbbyyTestTask.Core;
using AbbyyTestTask.Core.Builders;
using AbbyyTestTask.Core.Handlers;
using AbbyyTestTask.Core.Parsers;
using AbbyyTestTask.Implementation.Builders.Default;
using AbbyyTestTask.Implementation.Parsers.Default;

namespace AbbyyTestTask.Implementation.Handlers.Default
{
    public sealed class DefaultCanonicalEquationHandler : ICanonicalEquationHandler
    {
        public IEquationParser EquationParser { get; private set; } = new DefaultEquationParser();
        public ICanonicalEquationBuilder CanonicalEquationBuilder { get; private set; } = new DefaultCanonicalEquationBuilder();

        public DefaultCanonicalEquationHandler()
        {

        }

        public DefaultCanonicalEquationHandler(IEquationParser equationParser, ICanonicalEquationBuilder canonicalEquationBuilder)
        {
            EquationParser = equationParser ?? throw new ArgumentNullException(nameof(equationParser));
            CanonicalEquationBuilder = canonicalEquationBuilder ?? throw new ArgumentNullException(nameof(canonicalEquationBuilder));
        }

        public CanonicalEquation Processing(string s)
        {
            return CanonicalEquationBuilder.BuildCanonicalEquation(EquationParser.Parse(s));
        }
    }
}
