﻿using AbbyyTestTask.Core;
using AbbyyTestTask.Core.Builders;
using System.Collections.Generic;
using System.Linq;

namespace AbbyyTestTask.Implementation.Builders.Default
{
    public sealed class DefaultCanonicalEquationBuilder : ICanonicalEquationBuilder
    {
        public CanonicalEquation BuildCanonicalEquation(ICollection<EquationToken> tokens)
        {
            var canonicalTokens = tokens
                .GroupBy(t => t.ToString())
                .Select(t => new EquationToken(t.Sum(c => c.Coefficient), t.First().Variables))
                .Where(t => t.Coefficient != 0)
                .OrderBy(t => t, EquationToken.Comparer)
                .ToList();

            return new CanonicalEquation(canonicalTokens);
        }
    }
}
